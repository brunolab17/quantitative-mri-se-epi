# Quantitative MRI - SE EPI

Reconstruction code/demo for quantitative MRI

## Getting started

A tar file 'demo.tar' is provided here, it can be be downloaded and restored (in Linux) with:
tar -xvf demo.tar

Please edit the string 'stemdir' and the directory names listed in the file 'inputdatasets.m', so they may properly point to where 'demo.tar' was unpacked on your computer.

In matlab, type 'FitQuant' and the demo should run. In its current form the code takes some time to execute, some patience may be needed. Results will take the form of 4 .mat files appearing in the same directory as FitQuant.m: the T1, T2, M0 and flip angle maps. The present data were obtained using a multi-compartment agar gel phantom, with varying concentrations of a gadoterate meglumine contrast agent.

A file 'PROTOCOL.pdf' is also provided. It describes the product pulse sequences and parameter settings that were used, so that you could build your own protocol and acquire your own datasets. Alternately, a protocol (*.exar) file could be sent, please contact Bruno Madore at bruno@bwh.harvard.edu if interested.

Creating masks to suppress background voxels can be a tricky affair; if, in your application, too many background voxels remain bright in the parameters maps, or if on the contrary non-background voxels get inadvertantly zeroed, you may try adjusting the following mask-related parameters in FitQuant.m: mask_perc, mask_frac, maskb1_perc, maskb1_frac and the T2-based threshold setting.

If you find ways to make this code better and/or faster, please share these improvements back with us. If you use this approach and/or code as part of your work please reference:
Madore B, Jerosch-Herold M, Chiou Jr-Y, Cheng C-C, Guenette JP, Mihai G. A relaxometry method that emphasizes practicality and availability. Submitted for publication to Magn Reson Med.

The present code is only to be used for research purposes, along with proper IRB protocol and proper informed consent, and is not for clinical use.

## Version date

Most of the Matlab code provided here was written during the pandemic, in 2020-21, and it was last updated and uploaded here in 2022.


